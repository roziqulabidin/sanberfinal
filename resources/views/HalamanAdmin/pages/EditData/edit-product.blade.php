@extends('HalamanAdmin/master')

@section('isi')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Product</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <h3>Edit Product</h3>
                                @foreach ($products as $prd)
                                    <form method="post" action="/Admin/EditProduct/Update">
                                        @csrf
                                        <input type="hidden" name="id_prd" value="{{ $prd->id_prd }}">
                                        <div class="form-group">
                                            <label>Product Category</label>
                                            <input type="text" name="ctg_id" value="{{ $prd->ctg_id }}"
                                                class="form-control" placeholder="Product Category" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control select2" style="width: 100%;" name="ctg_id">
                                                <option value="">Choose Category</option>
                                                @foreach ($categories as $item)
                                                    @if ($item->ctg_id == $prd->ctg_id)
                                                        <option value="{{ $prd->ctg_id }}" selected>{{ $item->ctg_name }}
                                                        </option>
                                                    @else
                                                        <option value="{{ $item->ctg_id }}">{{ $item->ctg_name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Image</label>
                                            <input type="text" name="img_prd" value="{{ $prd->img_prd }}"
                                                class="form-control" placeholder="Image" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" name="nama_prd" value="{{ $prd->nama_prd }}"
                                                class="form-control" placeholder="Name" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input type="text" name="hrg_prd" value="{{ $prd->hrg_prd }}"
                                                class="form-control" placeholder="Price" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>Warranty</label>
                                            <input type="text" name="garansi_prd" value="{{ $prd->garansi_prd }}"
                                                class="form-control" placeholder="Warranty" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>Processor</label>
                                            <input type="text" name="processor_prd" value="{{ $prd->processor_prd }}"
                                                class="form-control" placeholder="Processor" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>Memory</label>
                                            <input type="text" name="memory_prd" value="{{ $prd->memory_prd }}"
                                                class="form-control" placeholder="Memory" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>Storage</label>
                                            <input type="text" name="storage_prd" value="{{ $prd->storage_prd }}"
                                                class="form-control" placeholder="Storage" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>VGA</label>
                                            <input type="text" name="graphic_prd" value="{{ $prd->graphic_prd }}"
                                                class="form-control" placeholder="VGA" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>Display</label>
                                            <input type="text" name="display_prd" value="{{ $prd->display_prd }}"
                                                class="form-control" placeholder="Display" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>Operating System</label>
                                            <input type="text" name="os_prd" value="{{ $prd->os_prd }}"
                                                class="form-control" placeholder="Operating System" required="">
                                        </div>
                                        <div class="form-group text-right">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>
                                                Update Data</button>
                                        </div>
                                    </form>
                                @endforeach
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection
