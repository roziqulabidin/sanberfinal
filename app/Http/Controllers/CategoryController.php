<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryCRUD;

class CategoryController extends Controller
{
    public function CategoryShow()
    {
        $categories['categories'] = CategoryCRUD::all();
        return view('HalamanAdmin.pages.category', ['categories'=>$categories]);
    }

    public function CategoryEdit($ctg_id)
    {
        $categories = CategoryCRUD::select()->where('ctg_id', $ctg_id)->get();
        return view('HalamanAdmin.pages.EditData.edit-category', compact('categories'));
    }

    public function CategoryUpdate(Request $request)
    {
        $categories = CategoryCRUD::where('ctg_id', $request->ctg_id)->update([
            'ctg_name' => $request->ctg_name,
        ]);
        return redirect('/Admin/Category/Show')->with('success', 'Data berhasil diupdate ! !');
    }

    public function CategoryAdd()
    {
        return view('HalamanAdmin.pages.CreateData.create-category');
    }

    public function CategorySave(Request $request)
    {
        $categories = CategoryCRUD::create([
            'ctg_name' => $request->ctg_name,
        ]);
        return redirect('/Admin/Category/Show')->with('success', 'Data berhasil ditambahkan ! !');
    }

}
